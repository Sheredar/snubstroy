init("slider-wrapper") 

function init(parentname) {   
  let pointbox =  $('<div>', {class: 'pagination'})  
  $('.' + parentname).append(pointbox) 
  let right = $('.' + parentname + ' .right')
  let left = $('.' + parentname + ' .left')
  let slides = $('.' + parentname + ' .slide')
  let point =  $('<div>', {class: 'point'})
  pointscreating(slides,point,parentname)
  let points = $('.' + parentname + ' .point')  
  let e = 0 
  $(slides[0]).addClass('active')
  $(points[0]).addClass('active')   

  let int = setInterval(function() {    
    goright(slides,parentname)
    pointcheck(slides,points,parentname)  
  },3000)  

  right.click(function(){
  clearInterval(int)   
  goright(slides,parentname)
  pointcheck(slides,points,parentname)     
  })

  left.click(function(){
  clearInterval(int)  
  goleft(slides,parentname)
  pointcheck(slides,points,parentname) 
  })

  points.click(function(e){
  clearInterval(int) 
  pointschanging(slides,points,e,parentname)  
  })
  
}

function pointcheck(slides,points,parentname) {  
  $(points).removeClass('active') 
  let k = slides.index($('.' + parentname + ' .slide.active'))  
  $(points[k]).addClass('active')   
}

function goright(slides,parentname) {
  let k = slides.index($('.' + parentname + ' .slide.active'))
  if (k<slides.length-1) {
      $(slides[k]).removeClass('active')
      $(slides[k+1]).addClass('active')
      k=k+1
    }
    else {      
      $(slides[k]).removeClass('active')     
      $(slides[0]).addClass('active')
      k=0      
    }    
}

function goleft(slides,parentname) {
  let k = slides.index($('.' + parentname + ' .slide.active'))
  if (k>0) {
      $(slides[k]).removeClass('active')
      $(slides[k-1]).addClass('active')
      k=k-1
    }
    else {      
      $(slides[k]).removeClass('active')      
      $(slides[slides.length-1]).addClass('active')
      k=slides.length-1      
    }   
}

function pointschanging(slides,points,e,parentname) {  
  $(points).removeClass('active')  
  $(e.target).addClass('active')  
  slides.removeClass('active') 
  let k = $(points).index($('.' + parentname + ' .point.active'))  
  $(slides[k]).addClass('active')  
}

function pointscreating(slides,point,parentname) {  
  for (let i = 0; i < slides.length; i++)       
    point.clone().appendTo($('.' + parentname + ' .pagination'))         
}














